﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Cabecao.EFMembershipProvider")]
[assembly: AssemblyDescription("Provedor de Membership usando Entity Framework de Omid Mafakher, com suporte à Webmatrix removido e possibilidade de uso de E-mail como login")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Cabecao.EFMembershipProvider")]
[assembly: AssemblyCopyright("Copyright © 2011, 2017 by Omid Mafakher & Luiz Figueiredo")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("93b7890a-2f6d-4401-b2b0-00f08540afc9")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.3.5")]
[assembly: AssemblyFileVersion("1.1.3.5")]
