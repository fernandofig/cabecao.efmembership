﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cabecao.Web.Security.Mapper {
    public enum RoleColumnType {

        Application,
        RoleID,
        RoleName,
        CreateOn

    }
}
