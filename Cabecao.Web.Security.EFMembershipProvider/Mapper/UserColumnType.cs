﻿
namespace Cabecao.Web.Security.Mapper {
	public enum UserColumnType {
		UserID,
		Username,
		Email,
		UsernameAndEmail,
		IsAnonymous,
		LastActivityDate,
		Password,
		PasswordFormat,
		PasswordSalt,
		PasswordQuestion,
		PasswordAnswer,
		IsApproved,
		CreateOn,
		LastLoginDate,
		LastPasswordChangedDate,
		LastLockoutDate,
		FailedPasswordAttemptCount,
		FailedPasswordAttemptWindowStart,
		FailedPasswordAnswerAttemptCount,
		FailedPasswordAnswerAttemptWindowStart,
		Comment,
		IsLockedOut,
		ProviderVerification,
		ProviderVerificationExpireOn,
		Application
	}
}
